package com.uptechpay.houseagency.network_request.classes

import com.google.gson.annotations.SerializedName

class Errors(

    @SerializedName("errors")
    val errors: String
)
