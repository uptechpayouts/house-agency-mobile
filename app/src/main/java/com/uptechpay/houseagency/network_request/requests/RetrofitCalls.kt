package com.uptechpay.houseagency.network_request.requests

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.util.Log
import com.uptechpay.houseagency.MainActivity
import com.uptechpay.houseagency.authentication.Login
import com.uptechpay.houseagency.helper_class.CustomDialogToast
import com.uptechpay.houseagency.helper_class.SharedPreferenceStorage
import com.uptechpay.houseagency.network_request.builder.RetrofitBuilder.apiService
import com.uptechpay.houseagency.network_request.classes.SuccessLogin
import com.uptechpay.houseagency.network_request.classes.SuccessRegister
import com.uptechpay.houseagency.network_request.classes.UserLogin
import com.uptechpay.houseagency.network_request.classes.UserRegister
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class RetrofitCalls {

    var customDialogToast = CustomDialogToast()

    fun registerUser(context: Context, userRegister: UserRegister){

        CoroutineScope(Dispatchers.Main).launch {

            val job = Job()
            CoroutineScope(Dispatchers.IO + job).launch {

                startRegistration(context, userRegister)
            }.join()
        }

    }
    private suspend fun startRegistration(context: Context, userRegister: UserRegister) {

        val job1 = Job()
        CoroutineScope(Dispatchers.Main + job1).launch {

            val progressDialog = ProgressDialog(context)
            progressDialog.setTitle("Please wait..")
            progressDialog.setMessage("Registration on progress..")
            progressDialog.show()

            var messageToast = ""
            val job = Job()

            CoroutineScope(Dispatchers.IO + job).launch {

                val apiInterface = apiService.registerUser(userRegister)
                apiInterface.enqueue(object : Callback<SuccessRegister> {
                    override fun onResponse(call: Call<SuccessRegister>, response: Response<SuccessRegister>) {

                        CoroutineScope(Dispatchers.Main).launch { progressDialog.dismiss() }

                        if (response.isSuccessful) {

                            //                        Log.e("-*-*-*-*success ", "-*-*-*-*-")
                            //                        println("-**-*-*-* ${response.body()}")
                            messageToast = "You have been registered successfully."

                            val intent = Intent(context, Login::class.java)
                            context.startActivity(intent)

                        } else {

                            val code = response.code()

                            val objectError = JSONObject(response.errorBody()!!.string())

                            val keys: Iterator<String> = objectError.keys()

                            var message = ""
                            while (keys.hasNext()) {
                                val key = keys.next()

                                message += if (objectError.get(key) is String){

                                    val finalString = objectError.get(key).toString()
                                    "Check on the following: \n $key : $finalString \n \n"
                                }else{
                                    //Error is not a String but a list

                                    val size = objectError.get(key).toString().length.toString()
                                    val finalString = objectError.get(key).toString().substring(1, size.toInt() -1)

                                    "Check on the following: \n $key : $finalString \n \n"

                                }


                            }
                            messageToast = message


                        }
                        CoroutineScope(Dispatchers.Main).launch { customDialogToast.CustomDialogToast(context as Activity, messageToast) }


                    }

                    override fun onFailure(call: Call<SuccessRegister>, t: Throwable) {
                        Log.e("-*-*error ", t.localizedMessage)
                        messageToast = "There is something wrong. Please try again"
                        CoroutineScope(Dispatchers.Main).launch { customDialogToast.CustomDialogToast(context as Activity, messageToast) }

                        progressDialog.dismiss()
                    }
                })

            }.join()


        }

    }

    fun loginUser(context: Context, userLogin: UserLogin){

        CoroutineScope(Dispatchers.Main).launch {

            val job = Job()
            CoroutineScope(Dispatchers.IO + job).launch {

                startLogin(context, userLogin)

            }.join()
        }

    }
    private suspend fun startLogin(context: Context, userLogin: UserLogin) {

        val sharedPreferenceStorage = SharedPreferenceStorage(context, "Agency")

        var isLoggedIn  = false
        val job1 = Job()
        CoroutineScope(Dispatchers.Main + job1).launch {

            val progressDialog = ProgressDialog(context)
            progressDialog.setTitle("Please wait..")
            progressDialog.setMessage("Login in progress..")
            progressDialog.show()

            var messageToast = ""
            val job = Job()
            CoroutineScope(Dispatchers.IO + job).launch {

                val apiInterface = apiService.loginUser(userLogin)
                apiInterface.enqueue(object : Callback<SuccessLogin> {
                    override fun onResponse(call: Call<SuccessLogin>, response: Response<SuccessLogin>) {

                        CoroutineScope(Dispatchers.Main).launch { progressDialog.dismiss() }

                        if (response.isSuccessful) {
                            isLoggedIn = true
                            messageToast = "You have been logged in successfully."

                            val intent = Intent(context, MainActivity::class.java)
                            context.startActivity(intent)

                            val refreshToken = response.body()?.refresh.toString()
                            val accessToken = response.body()?.access.toString()
                            val emailLog = response.body()?.email.toString()
                            val rolesList = response.body()?.roles


                            val hashMap1 = HashMap<String, String>()
                            hashMap1["email"] = emailLog
                            hashMap1["refresh"] = refreshToken
                            hashMap1["accessToken"] = accessToken
                            if (rolesList != null) {
                                for (roles in rolesList) {
                                    hashMap1["roles${roles.toString()}"] = roles.toString()
                                }
                            }
                            sharedPreferenceStorage.saveData(hashMap1, "profile")

                        } else {

                            val code = response.code()
                            val message = response.errorBody().toString()
                            val jObjError = JSONObject(response.errorBody()!!.string())

                            for (items in jObjError.toString()) {

                                Log.e("-*-*-* ", items.toString())

                            }

                            isLoggedIn = false
                            messageToast = "Failed to login. Please try again"


                        }
                        CoroutineScope(Dispatchers.Main).launch { customDialogToast.CustomDialogToast(context as Activity, messageToast) }

                        Log.e("-*-*-*-*5 ", isLoggedIn.toString())

                    }

                    override fun onFailure(call: Call<SuccessLogin>, t: Throwable) {
//                        Log.e("-*-*error ", t.localizedMessage)
                        isLoggedIn = false
                        messageToast = "There is something wrong. Please try again"
                        CoroutineScope(Dispatchers.Main).launch { customDialogToast.CustomDialogToast(context as Activity, messageToast) }

                        progressDialog.dismiss()
                    }
                })



            }.join()




        }


    }


}

