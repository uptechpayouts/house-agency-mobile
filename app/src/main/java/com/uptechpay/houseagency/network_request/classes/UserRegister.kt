package com.uptechpay.houseagency.network_request.classes

import android.provider.ContactsContract
import com.google.gson.annotations.SerializedName

class UserRegister (

    val email: String,
    val phone_number: String,
    val password: String,
    val password2: String,
    val roles: List<Int>

)