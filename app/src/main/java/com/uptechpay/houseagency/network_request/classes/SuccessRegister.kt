package com.uptechpay.houseagency.network_request.classes

import com.google.gson.annotations.SerializedName

class SuccessRegister(

    @SerializedName("email")
    val email: String
)
