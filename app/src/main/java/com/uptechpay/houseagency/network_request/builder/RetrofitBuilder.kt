package com.uptechpay.houseagency.network_request.builder

import com.uptechpay.houseagency.network_request.`interface`.Interface
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitBuilder {

    private const val BASE_URL = "http://192.168.214.102:8001"

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build() //Doesn't require the adapter
    }

    val apiService: Interface = getRetrofit().create(Interface::class.java)
}