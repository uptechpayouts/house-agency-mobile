package com.uptechpay.houseagency.network_request.`interface`

import com.uptechpay.houseagency.network_request.classes.SuccessLogin
import com.uptechpay.houseagency.network_request.classes.SuccessRegister
import com.uptechpay.houseagency.network_request.classes.UserLogin
import com.uptechpay.houseagency.network_request.classes.UserRegister
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface Interface {

    @POST("/auth/register/")
    fun registerUser(@Body userRegister: UserRegister): Call<SuccessRegister>

    @POST("/auth/login/")
    fun loginUser(@Body user: UserLogin): Call<SuccessLogin>

}