package com.uptechpay.houseagency.network_request.classes

import com.google.gson.annotations.SerializedName

class SuccessLogin(

    @SerializedName("refresh")
    val refresh: String,

    @SerializedName("access")
    val access: String,

    @SerializedName("email")
    val email: String,

    @SerializedName("roles")
    val roles: List<Int>
)
