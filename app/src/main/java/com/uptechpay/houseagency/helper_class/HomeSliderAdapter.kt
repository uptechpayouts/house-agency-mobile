package com.uptechpay.houseagency.helper_class

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.uptechpay.houseagency.R
import com.uptechpay.manager.AddProperty
import java.util.*

class HomeSliderAdapter(context: Context): PagerAdapter() {

    private var context: Context = context
    private lateinit var  layoutInflater: LayoutInflater

    var slideImages = intArrayOf(
            R.drawable.image,
            R.drawable.image,
            R.drawable.image)

    var slideHeadings = arrayOf(
            "Welcome to KODDI",
            "Manage Your Real-estate Portfolio",
            "Manage Your Rented Apartment")

    var slideDescription = arrayOf(
            "We help you manage your rental and rented\n" +
                    "property from the comfort of your home",
            "Create and manage your portfolio online, see\n" +
                    "your occupancy rate pre house and income\n" +
                    "generated per unit.",
            "Tenants have easy access to the apartments\n" +
                    "agents, easily pay your apartment bills and find\n" +
                    "vacant houses incase you want to move")

    override fun getCount(): Int {

        return slideHeadings.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {

        return view == `object`
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = layoutInflater.inflate(R.layout.slide_layout, container, false)
        val imageView = view.findViewById<ImageView>(R.id.imageView)
        val tvTitle = view.findViewById<TextView>(R.id.tvTitle)
        val tvDescription = view.findViewById<TextView>(R.id.tvDescription)
        val btnAddProperty = view.findViewById<Button>(R.id.btnAddProperty)
        imageView.setImageResource(slideImages.get(position))
        view.setOnClickListener { view1: View? -> }

        tvTitle.text = slideHeadings[position]
        tvDescription.text = slideDescription[position]


        btnAddProperty.setOnClickListener { viewAll() }
        container.addView(view)
        return view
    }

    private fun viewAll() {
        val intent = Intent(context, AddProperty::class.java)
        context.startActivity(intent)
    }



}