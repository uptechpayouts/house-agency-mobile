package com.uptechpay.houseagency.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import com.uptechpay.houseagency.R
import com.uptechpay.houseagency.helper_class.PhoneStatus
import com.uptechpay.houseagency.helper_class.TextValidator
import com.uptechpay.houseagency.network_request.classes.UserRegister
import com.uptechpay.houseagency.network_request.requests.RetrofitCalls

class Register : AppCompatActivity() {


    private lateinit var etEmailAddress : EditText
    private lateinit var etPhoneNumber : EditText
    private lateinit var etPassword : EditText
    private lateinit var etConfirmPassword : EditText

    private var textValidator : TextValidator = TextValidator()
    private var mobilePhoneStatus : PhoneStatus = PhoneStatus()
    private var retrofitCalls : RetrofitCalls = RetrofitCalls()

    private lateinit var linearLogin: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        etEmailAddress = findViewById(R.id.etEmailAddress)
        etPhoneNumber = findViewById(R.id.etPhoneNumber)
        etPassword = findViewById(R.id.etPassword)
        etConfirmPassword = findViewById(R.id.etConfirmPassword)
        linearLogin = findViewById(R.id.linearLogin)

        etEmailAddress.addTextChangedListener(emailTextWatcher)
        etPhoneNumber.addTextChangedListener(phoneNumberTextWatcher)
        etPassword.addTextChangedListener(passwordTextWatcher)
        etConfirmPassword.addTextChangedListener(confirmPasswordTextWatcher)

        linearLogin.setOnClickListener {

            val intent = Intent(this, Login::class.java)
            startActivity(intent)

        }


        findViewById<ImageButton>(R.id.imgbtnSignIn).setOnClickListener {

            val emailAddress = etEmailAddress.text.toString()
            val phoneNumber = etPhoneNumber.text.toString()
            val passWord = etPassword.text.toString()
            val confirmPassword = etConfirmPassword.text.toString()

            val isEmailValid = textValidator.isEmailValid(emailAddress)

            val phoneStatus = textValidator.isPhoneNumberValid(phoneNumber)
            val isPhoneValid = phoneStatus.first

            val passwordStatus = textValidator.isPasswordStrong(passWord)
            val isPasswordStrong = passwordStatus.first

            val isPasswordSame = textValidator.isPasswordSame(passWord, confirmPassword)

            if (isEmailValid && isPhoneValid  && isPasswordSame){

                //Everything seems okay
                val list = ArrayList<Int>()

                val userRegister = UserRegister(
                    emailAddress, phoneNumber, passWord, confirmPassword, list
                )

                retrofitCalls.registerUser(this, userRegister)



            }else{

                if (!isEmailValid)
                    etEmailAddress.error = "Email is invalid"
                else
                    etEmailAddress.error = null

                val phoneCount = phoneStatus.second

                if (!isPhoneValid)
                    etPhoneNumber.error = "Phone is invalid. $phoneCount/10"
                else
                    etEmailAddress.error = null

                val passwordCount = passwordStatus.second
                val strength = passwordStatus.third

                if (!isPasswordStrong) {
                    etPassword.error = "Password is $strength. $passwordCount/6 "
                    etConfirmPassword.isEnabled = false
                    etConfirmPassword.setText("")
                }else {
                    etConfirmPassword.isEnabled = true
                    etPassword.error = null
                }

                if (!isPasswordSame)
                    etConfirmPassword.error = "Passwords don't match."
                else
                    etConfirmPassword.error = null


            }

        }


    }

    private val emailTextWatcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            val isEmailValid = textValidator.isEmailValid(s.toString())
            if (!isEmailValid)
                etEmailAddress.error = "Email is invalid"
            else
                etEmailAddress.error = null

        }

    }
    private val phoneNumberTextWatcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            val phoneStatus = textValidator.isPhoneNumberValid(s.toString())
            val isPhoneValid = phoneStatus.first
            val phoneCount = phoneStatus.second

            if (!isPhoneValid)
                etPhoneNumber.error = "Phone is invalid. $phoneCount/10"
            else
                etEmailAddress.error = null

        }

    }
    private val passwordTextWatcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            val passwordStatus = textValidator.isPasswordStrong(s.toString())
            val isPasswordStrong = passwordStatus.first
            val passwordCount = passwordStatus.second
            val strength = passwordStatus.third

            if (!isPasswordStrong) {
                etPassword.error = "Password is $strength. $passwordCount/8 "
                etConfirmPassword.isEnabled = true
                etConfirmPassword.setText("")
            }else {
                etConfirmPassword.isEnabled = true
                etPassword.error = null
            }

        }

    }
    private val confirmPasswordTextWatcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            val password = etPassword.text.toString()
            val isPasswordSame = textValidator.isPasswordSame(password, s.toString())
            if (!isPasswordSame)
                etConfirmPassword.error = "Passwords don't match."
            else
                etConfirmPassword.error = null

        }

    }


}