package com.uptechpay.houseagency.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import com.uptechpay.houseagency.R
import com.uptechpay.houseagency.helper_class.TextValidator
import com.uptechpay.houseagency.network_request.classes.UserLogin
import com.uptechpay.houseagency.network_request.requests.RetrofitCalls

class Login : AppCompatActivity() {

    private lateinit var linearCreate : TextView
    private lateinit var etEmailAddress : EditText
    private lateinit var etPassword : EditText
    private var textValidator : TextValidator = TextValidator()
    private var retrofitCalls : RetrofitCalls = RetrofitCalls()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        etEmailAddress = findViewById(R.id.etEmailAddress)
        etPassword = findViewById(R.id.etPassword)
        etEmailAddress.addTextChangedListener(emailTextWatcher)
        etPassword.addTextChangedListener(passwordTextWatcher)

        linearCreate = findViewById(R.id.linearCreate)
        linearCreate.setOnClickListener {

            val intent = Intent(this, Register::class.java)
            startActivity(intent)

        }

        findViewById<ImageButton>(R.id.imgBtnSignIn).setOnClickListener {

            val emailAddress = etEmailAddress.text.toString()
            val passWord = etPassword.text.toString()

            val isEmailValid = textValidator.isEmailValid(emailAddress)

            val passwordStatus = textValidator.isPasswordStrong(passWord)
            val isPasswordStrong = passwordStatus.first

            if (isEmailValid  && isPasswordStrong){

                val userLogin = UserLogin(emailAddress, passWord)
                retrofitCalls.loginUser(this, userLogin)



            }else{

                if (!isEmailValid)
                    etEmailAddress.error = "Email is invalid"
                else
                    etEmailAddress.error = null

                val passwordCount = passwordStatus.second
                val strength = passwordStatus.third

                if (!isPasswordStrong) {
                    etPassword.error = "Password is $strength. $passwordCount/6 "
                }else {
                    etPassword.error = null
                }


            }


        }

    }

    private val emailTextWatcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            val isEmailValid = textValidator.isEmailValid(s.toString())
            if (!isEmailValid)
                etEmailAddress.error = "Email is invalid"
            else
                etEmailAddress.error = null

        }

    }
    private val passwordTextWatcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            val passwordStatus = textValidator.isPasswordStrong(s.toString())
            val isPasswordStrong = passwordStatus.first
            val passwordCount = passwordStatus.second
            val strength = passwordStatus.third

            if (!isPasswordStrong) {
                etPassword.error = "Password is $strength. $passwordCount/8 "
            }else {
                etPassword.error = null
            }

        }

    }

}