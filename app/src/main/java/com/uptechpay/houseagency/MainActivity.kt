package com.uptechpay.houseagency

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.widget.LinearLayout
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.uptechpay.houseagency.helper_class.HomeSliderAdapter

class MainActivity : AppCompatActivity() {

    private lateinit var mSliderViewPager: ViewPager
    private lateinit var mDotLayout: LinearLayout
    private lateinit var mDots: Array<TextView?>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mSliderViewPager = findViewById(R.id.slideViewPager)
        mDotLayout = findViewById(R.id.dotsLayout)

        setUpViewPager()

    }

    private fun setUpViewPager() {
        val sliderAdapter = HomeSliderAdapter(this)
        mSliderViewPager.adapter = sliderAdapter
        addDotsIndicator(0)
        mSliderViewPager.addOnPageChangeListener(viewListener)
    }

    fun addDotsIndicator(position: Int) {
        mDots = arrayOfNulls(3)
        mDotLayout.removeAllViews()
        for (i in mDots.indices) {
            mDots[i] = TextView(this)
            mDots[i]!!.text = Html.fromHtml("&#8226;")
            mDots[i]!!.textSize = 35f
            mDots[i]!!.setTextColor(resources.getColor(R.color.colorPrimary))
            mDotLayout.addView(mDots[i])
        }
        if (mDots.isNotEmpty()) {
            mDots[position]!!.setTextColor(resources.getColor(R.color.colorGray))
        }
    }

    var viewListener: ViewPager.OnPageChangeListener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
        override fun onPageSelected(position: Int) {
            addDotsIndicator(position)
        }

        override fun onPageScrollStateChanged(state: Int) {}
    }

}