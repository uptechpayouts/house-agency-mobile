package com.uptechpay.manager

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.uptechpay.houseagency.MainActivity
import com.uptechpay.houseagency.R

class AddPropertyCaretaker : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_property_caretaker)

        findViewById<Button>(R.id.btnSave).setOnClickListener {

            val intent = Intent(this, MainActivityManager::class.java)
            startActivity(intent)
            finish()

        }

    }
}